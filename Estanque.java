 public class Estanque {
     //el estanque se inicializa en 32 litros según guía
     private double volumen = 32;
     //setters y getters de volumen
     public double getVolumen() {
         return volumen;
     }

     public void setVolumen(double volumen) {
         this.volumen = volumen;
     }
 }
