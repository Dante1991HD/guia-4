import java.util.ArrayList;
import java.util.Random;

public class Vehiculo {
    //atributos adquiridos de la unión de objetos independientes
    private Motor motor;
    private Velocimetro velocimetro;
    private ArrayList < Ruedas > rueda = new ArrayList < Ruedas > ();
    private Estanque estanque;
    private boolean encendido = false;
    //setters y getters de los atributos del auto
    public Motor getMotor() {
        return motor;
    }

    public void setMotor(Motor motor) {
        this.motor = motor;
    }

    public Velocimetro getVelocimetro() {
        return velocimetro;
    }

    public void setVelocimetro(Velocimetro velocimetro) {
        this.velocimetro = velocimetro;
    }

    public ArrayList < Ruedas > getRueda() {
        return rueda;
    }

    public void setRueda(Ruedas rueda) {
        this.rueda.add(rueda);
    }

    public Estanque getEstanque() {
        return estanque;
    }

    public void setEstanque(Estanque estanque) {
        this.estanque = estanque;
    }

    public boolean isEncendido() {
        return encendido;
    }

    public void setEncendido() {
        if (encendido) {
            this.encendido = false;
            velocimetro.setVelocidad(0);
        } else {
            this.estanque.setVolumen(this.estanque.getVolumen() * .99);
            this.encendido = true;
        }
    }

    public void movimiento() {
        double distancia;
        if (this.isEncendido()) {
            if (this.getEstanque().getVolumen() > 0) {
                /*Para el calculo de la distancia se considera distancia = velocidad * tiempo
                 * tomando en cuenta kilometros por hora como velocidad y segundos como tiempo, 
                 * se adapta el tiempo presentado por guía de segundos a horas, para trabajar la misma unidad, por ello se divide por 3600,
                 * ademas por temas de testeo, ya que eventos se desencadenan a mayor distancia,
                 *  se consideró minutos por lo que se deja a discreción del tester*/
                //distancia = this.velocimetro.getVelocidad() * new Random().nextInt(10 + 1)/3600;
                distancia = this.velocimetro.getVelocidad() * new Random().nextInt(10 + 1) / 60;
                this.velocimetro.setDistancia(distancia);
                this.estanque.setVolumen(this.estanque.getVolumen() - (distancia * velocimetro.getVelocidad() / motor.consumo()));
            } else {
                this.setEncendido();
                System.out.println("Se apaga el motor por falta de vencina");
            }
        }
        this.chequeoRueda();
    }
    //muestra el estado de las ruedas y según su estado las cambía por una nueva
    public void chequeoRueda() {
        for (Ruedas r: this.rueda) {
            int desgaste = r.getDesgaste() * (100 - (new Random().nextInt(10) + 1)) / 100;
            r.setDesgaste(desgaste);
            if (r.getDesgaste() <= 10) {
                System.out.println("Se apaga el motor para cambiar la rueda");
                this.setEncendido();
                this.rueda.set(this.rueda.indexOf(r), new Ruedas());
                System.out.println("La rueda ha sido cambiada");
            }
        }
    }
    //imprime el estado de los objetos que componen el auto según lo pedido por la guía
    public void reporteAcual() {
        if (this.encendido) {
            System.out.print("El auto se encuentra encendido ");
        } else {
            System.out.print("El auto se encuentra apagado ");
        }
        //las salidas las dejé kilometros por hora y kilometros independiente del testeo, esto producto a que me hace más sentido y según lo explicado más atras
        System.out.printf("ha recorrido %.2f kilometros", this.velocimetro.getDistancia());
        System.out.print(" se mueve a " + this.velocimetro.getVelocidad() + " kilometros por hora ");
        System.out.printf("Posee %.2f litros y las condiciones de sus ruedas son: ", this.estanque.getVolumen());
        for (Ruedas r: this.rueda) {
            System.out.print(r.getDesgaste() + "% ");
        }
        System.out.println();
    }
}
