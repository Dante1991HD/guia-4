public class Ruedas {
    //se inicializa el desgaste de las ruedas a salud perfecta
    private int desgaste = 100;
    //setters y getters de desgaste
    public int getDesgaste() {
        return desgaste;
    }

    public void setDesgaste(int desgaste) {
        this.desgaste = desgaste;
    }

}
