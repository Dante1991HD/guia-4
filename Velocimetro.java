public class Velocimetro {
    //se declara la velocidad maxima
    private int velocidad;
    private static int velocidadMaxima = 120;
    private double distancia;
    //setters y getters de velocidad y velocidadmaxima
    public int getVelocidad() {
        return velocidad;
    }
    //set velocidad controla que no pase el maximo de 120 kmh
    public void setVelocidad(int velocidad) {
        if (this.velocidad <= getVelocidadMaxima() - 10) {
            this.velocidad += velocidad;
        } else {
            this.velocidad = getVelocidadMaxima();
        }
    }
    public int getVelocidadMaxima() {
        return velocidadMaxima;
    }
    //setters y getters de distancia
    public double getDistancia() {
        return distancia;
    }
    //la distancia siempre aumenta según guía
    public void setDistancia(double distancia) {
        this.distancia += distancia;
    }
}
