public class Main {

	public static void main(String[] args) {
		Vehiculo mazda3 = new Vehiculo();
		Motor motor = new Motor();
		Estanque estanque = new Estanque();
		Velocimetro velocimetro = new Velocimetro();
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
public class Main {

    public static void main(String[] args) throws IOException {
        //se crean los objetos para el vehiculo 
        Vehiculo mazda3 = new Vehiculo();
        Motor motor = new Motor();
        Estanque estanque = new Estanque();
        Velocimetro velocimetro = new Velocimetro();
        BufferedReader bufferReader = new BufferedReader(new InputStreamReader(System.in));
        String avanzar;
        String prender;
        for (int i = 0; i < 4; i++) {
            mazda3.setRueda(new Ruedas());
        }
        //Se entregan los objetos ya creados como atributos
        mazda3.setMotor(motor);
        mazda3.setEstanque(estanque);
        mazda3.setVelocimetro(velocimetro);
        mazda3.reporteAcual();
        //Ciclo general del programa, se detiene al no quedar combustible como lo pide la guía
        while (mazda3.getEstanque().getVolumen() > 0) {
            //de estar encendido se puede mover con coste de combustible
            if (mazda3.isEncendido()) {
                System.out.println("Para avanzar presiones <1>, para apagar el motor otro número cualquiera");
                avanzar = bufferReader.readLine();
                //se moverá con un botón como lo pide la guía
                if (Integer.parseInt(avanzar) == 1) {
                    velocimetro.setVelocidad(10);
                    mazda3.movimiento();
                    mazda3.reporteAcual();
                } else {
                    //cualquier otro botón lo detendrá 
                    mazda3.setEncendido();
                }
            } else {
                //de estar apagado el vehiculo se podrá prender con un botón
                System.out.println("Para encender su auto presione <1>, dejar el auto otro número cualquiera");
                prender = bufferReader.readLine();
                System.out.println(prender);
                if (Integer.parseInt(prender) == 1) {
                    mazda3.setEncendido();
                    mazda3.reporteAcual();
                    prender = "0";
                } else {
                    mazda3.reporteAcual();
                    break;
                }
            }
        }
        System.out.println("Gracias por usar el simulador");
    }
}
