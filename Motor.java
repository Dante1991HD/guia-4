import java.util.Random;

public class Motor {

    private String cilindrada;
    private int consumo;
    //motor selecciona entre las 2 cilindradas
    Motor() {
        String cilindrada[] = new String[] {
            "1,2",
            "1,6"
        };
        this.cilindrada = cilindrada[new Random().nextInt(cilindrada.length)];
    }
    //getter de cilindrada
    public String getCilindrada() {
        return cilindrada;
    }
    //consumo indica el ratio de consumo según cilindrada y la retorna
    public int consumo() {
        if (this.getCilindrada() == "1,2") {
            this.consumo = 14 * 120;
        } else {
            this.consumo = 20 * 120;
        }
        return consumo;
    }
}
